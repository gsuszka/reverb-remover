import multiprocessing as mp
import os
from itertools import zip_longest
from typing import *

import numpy as np
from tqdm import tqdm

from src.consts import *
from src.htk_file import *

__all__ = [
    'features',
    'examples_count',
    'grouper',
]


def features(
        root: PathLike = D_FEATURES,
        only_paths: bool = False,
        only_count: bool = False,
) -> Generator[Union[Tuple[str], Tuple[HTKFile], int], None, None]:
    """Function generating feature info from *.mfcc files.
    If `only_paths' nor `only_count` are not set, function yields pair of {HTKFile} object in the following order:
    HTKFile object of base track, HTKFile object of processed (with reverb) track.
    
    Keyword Arguments:
        root {PathLike} --  the directory, where the mfcc files are located (default: {D_FEATURES})
        only_paths {bool} -- if set, yields pairs of paths in following order: path of base track, path of processed (with reverb) track (default: {False})
        only_count {bool} -- if set, yields count of mfcc files (default: {False})
    
    Returns:
        Generator[Union[Tuple[str], Tuple[HTKFile], int], None, None] -- [description]
    """
    count: int = 0
    for root, subdirs, filenames in os.walk(root):
        for filename in filenames:
            if filename.endswith(f'.{EXT_FEATURES}'):
                path = str(Path(root) / filename)
                path_processed = path.replace(DN_FEATURES, DN_FEATURES_PROCESSED)
                if not os.path.isfile(path_processed): print(path, 'missing processed equivalent')
                else:
                    both_paths = path, path_processed
                    count += 1
                    if not only_count:
                        yield both_paths if only_paths else tuple(map(HTKFile, both_paths))
    if only_count: yield count


def grouper(iterable: Iterable[Any], n: int, fillvalue: Any = None) -> Generator[Iterable, None, None]:
    """Groups iterable into n disjunctive slices.
    
    Arguments:
        iterable {iterable} -- object, which suppose to be sliced
        n {int} -- number of groups
    
    Keyword Arguments:
        fillvalue {[type]} -- value of elements topping up groups (default: {None})
    
    Returns:
        [Generator[Iterable, None, None] - generator of `n` groups
    """
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def _examples_count(args) -> int:
    """Help function for multiprocessing purposes."""
    paths, context_size = args
    return sum(len(HTKFile(path).data) - 2 * context_size for path in paths if path)


def examples_count(
        paths: Iterable[PathLike],
        context_size: int,
        name: str,
        print_out: bool = True,
        nproc: int = mp.cpu_count(),
) -> int:
    """Counts of all possible windows in mfcc files.
    
    Arguments:
        paths {Iterable[PathLike]} -- paths of mfcc files
        context_size {int} -- self explaining
        name {str} - category of paths
        print_out {bool} - determine whether print out the count
    
    Returns:
        {int} -- count of windows in files
    """
    with mp.Pool(nproc) as pool:
        args_gen = ((_paths, context_size) for _paths in grouper(paths, nproc))
        count = list(
            tqdm(
                pool.imap_unordered(_examples_count, args_gen),
                f'Counting {name} examples',
                len(paths) // nproc,
            ))
        count = sum(count)
        if print_out: print(f'Count of {name} examples:', count)
        return count
