import multiprocessing as mp
from enum import Enum, auto
from pathlib import Path
from typing import *

import keras as k
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tqdm import tqdm

from src.consts import *
from src.data import *
from src.htk_file import *
from src.ratio_callback import *

__all__ = [
    'AutoEncoder',
    'ModelType',
]


class ModelType(Enum):
    MLP = auto()
    LSTM = auto()


class AutoEncoder:
    default_encoder_layers_config = [
        dict(units=39, activation=k.activations.relu),
    ]
    default_decoder_layers_config = [
        dict(units=39, activation=k.activations.relu),
    ]

    def __init__(self,
                 model_type: ModelType,
                 optimizer: k.optimizers.Optimizer,
                 n_features: int = 39,
                 context_size: int = 10,
                 bs: int = 2048,
                 valid_ratio: float = 0.1,
                 encoder_layers_config: Iterable[Dict] = default_encoder_layers_config,
                 decoder_layers_config: Iterable[Dict] = default_decoder_layers_config,
                 nproc: int = mp.cpu_count()):
        """Autoencoder class

        Arguments:
            model_type {ModelType} -- [description]

        Keyword Arguments:
            n_features {int} -- number of features in one mfcc vector. (default: {39})
            context_size {int} -- number of vectors preceding and succeeding the center vector (default: {10})
            bs {int} -- batch size (default: {256})
            valid_ratio {float} -- ratio of validation data to training data (default: {0.1})
            encoder_layers_config {Iterable[Dict]} -- config of encoding layers (default: {default_encoder_layers_config})
            decoder_layers_config {Iterable[Dict]} -- config of decoding layers (default: {default_decoder_layers_config})
            nproc {int} -- max number of cores for this program
        """
        self.optimizer = optimizer
        self.n_features = n_features
        self.context_size = context_size
        self.encoder_layers_config = encoder_layers_config
        self.decoder_layers_config = decoder_layers_config
        self.nproc = nproc
        self.bs = bs
        data_paths = np.asarray(list(features(only_paths=True)))[:, 0][:1000]
        self.X_train, self.X_valid = train_test_split(data_paths, test_size=valid_ratio)
        self.X_train_count = examples_count(self.X_train, self.context_size, 'train', nproc=self.nproc)
        self.X_valid_count = examples_count(self.X_valid, self.context_size, 'validation', nproc=self.nproc)
        self.data_train = self.data_gen(self.X_train)
        self.data_valid = self.data_gen(self.X_valid)
        self.model: k.Model = {ModelType.LSTM: self.lstm_model, ModelType.MLP: self.mlp_model}[model_type]()

    @property
    def callbacks(self) -> List[k.callbacks.Callback]:
        """Callbacks used during training

        Returns:
            List[k.callbacks.Callback] -- list of keras callbacks
        """
        return [
            k.callbacks.ModelCheckpoint(str(FP_MODEL), monitor='val_loss', verbose=1, save_best_only=True),
            LogCallback(lambda: self.data_gen(self.X_valid, once=True), self.X_valid_count, self.bs, nproc=self.nproc)
        ]

    def fit(self, epochs: int = 1000):
        """Trains model

        Keyword Arguments:
            epochs {int} -- number of training epoch (default: {1000})
        """
        self.model.fit_generator(generator=self.data_train,
                                 epochs=epochs,
                                 steps_per_epoch=self.X_train_count // self.bs,
                                 validation_data=self.data_valid,
                                 validation_steps=self.X_valid_count // self.bs,
                                 verbose=1,
                                 callbacks=self.callbacks,
                                 shuffle=True,
                                 use_multiprocessing=True,
                                 workers=self.nproc)

    @staticmethod
    def euclidean_distance_loss(y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        """Calculates euclidean distance between two tensors.

        Arguments:
            y_true {tf.Tensor} -- ground truths
            y_pred {tf.Tensor} -- predicted values

        Returns:
            tf.Tensor -- result
        """
        return k.backend.sqrt(k.backend.sum(k.backend.square(y_pred - y_true), axis=-1))

    def data_gen_features(self, track_path: PathLike):
        """Yields concatenated (2*context size + 1) context vectors with 39 features in each and middle; w/out reverb.

        Arguments:
            track_path {str} -- track from which the vectors will be gathered
        """
        track_path = str(track_path)
        base_example = HTKFile(track_path)
        processed_example = HTKFile(track_path.replace(DN_FEATURES, DN_FEATURES_PROCESSED))
        for idx in range(self.context_size, len(base_example.data) - self.context_size):
            context = np.concatenate(processed_example.data[idx - self.context_size:idx + self.context_size + 1])
            expected = np.asarray(base_example.data[idx])
            yield context, expected

    def data_gen_tracks(self, data: Iterable[PathLike]):
        """Yields vectors from `data_gen_features` using many track paths.

        Arguments:
            data {Iterable[PathLike]} -- list of track paths
        """
        for track in data:
            yield from self.data_gen_features(track)

    def data_gen(
            self,
            data: Iterable[PathLike],
            bs: Optional[int] = None,
            once: bool = False,
    ) -> Generator[Tuple[np.array], None, None]:
        """Yields data as in `data_gen_features` for track paths in `data`.

        Arguments:
            data {Iterable[PathLike]} -- iterable object of track paths

        Keyword Arguments:
            bs {Optional[int]} -- optional batch size (default: {None})
            once {bool} -- determines, whether to run generator once or infinitely (default: {False})

        Returns:
            Generator[Tuple[np.array], None, None] -- generator of pairs of numpy arrays
        """
        bs = bs if bs else self.bs

        def empty():
            return np.empty((bs, (self.context_size * 2 + 1) * self.n_features)), np.empty((bs, self.n_features))

        while True:
            input_data, expected = empty()
            ids = np.random.randint(0, data.shape[0], data.shape[0])
            counter: int = 0
            for context, base_features in self.data_gen_tracks(data[ids]):
                input_data[counter] = context
                expected[counter] = base_features
                if (counter + 1) % bs == 0:
                    yield input_data, expected
                    input_data, expected = empty()
                    counter = -1
                counter += 1
            if self.once:
                break

    def mlp_encoder(self, input_layer: k.layers.Layer) -> k.layers.Layer:
        encoded = None
        for idx, layer_config in enumerate(self.encoder_layers_config):
            prev_layer = input_layer if idx == 0 else encoded
            encoded = k.layers.Dense(**layer_config)(prev_layer)
            # encoded = k.layers.Dropout(0.2)(encoded)
        return encoded

    def mlp_decoder(self, input_layer: k.layers.Layer) -> k.layers.Layer:
        decoded = None
        for idx, layer_config in enumerate(self.decoder_layers_config):
            prev_layer = input_layer if idx == 0 else decoded
            decoded = k.layers.Dense(**layer_config)(prev_layer)
        return decoded

    def mlp_model(self) -> k.Model:
        # INPUT LAYER
        input_layer = k.layers.Input(((2 * self.context_size + 1) * self.n_features,))

        encoded = self.mlp_encoder(input_layer)
        decoded = self.mlp_decoder(encoded)

        # AUTOENCODER
        autoencoder = k.Model(input_layer, decoded)
        autoencoder.compile(optimizer=self.optimizer, loss=self.euclidean_distance_loss, metrics=[])

        return autoencoder

    def lstm_model(self) -> k.Model:
        # TODO
        raise NotImplementedError
        return k.Model()
