import multiprocessing as mp
from pathlib import Path
from sys import stdout
from typing import *

import keras as k
import numpy as np
import tensorflow as tf
from tqdm import tqdm

from src.data import *

__all__ = ['LogCallback']


def mp_ratio(idx, model: k.Model, context, nfeatures, expected):
    stdout.write(f'Ratios {idx}\n')
    stdout.flush()
    predicted = model.predict(context)
    ratio = np.sqrt(np.sum(np.power(predicted - expected, 2), 1))
    context = context.reshape((-1, nfeatures))
    context = context[context.shape[0] // 2]
    ratio /= np.sqrt(np.sum(np.power(context - expected, 2), 1))
    stdout.write(f'Ratios {idx} DONE\n')
    stdout.flush()
    return ratio


class LogCallback(k.callbacks.Callback):

    def __init__(self,
                 validation_data_gen: Generator,
                 data_count: int,
                 bs: int,
                 nfeatures: int = 39,
                 nproc: int = mp.cpu_count(),
                 logdir='./logs'):
        self.logdir = logdir
        self.validation_data = validation_data_gen
        self.samples_seen: int = 0
        self.nfeatures = nfeatures
        self.data_count = data_count
        self.bs = bs
        self.nproc = nproc

    def set_model(self, model: k.Model):
        self.model = model
        self.writer = tf.summary.FileWriter(str(Path(self.logdir) / 'loss'))
        self.writer_ratio_avg = tf.summary.FileWriter(str(Path(self.logdir) / 'ratio_avg'))
        self.writer_ratio_min = tf.summary.FileWriter(str(Path(self.logdir) / 'ratio_min'))
        self.writer_ratio_max = tf.summary.FileWriter(str(Path(self.logdir) / 'ratio_max'))

    def on_batch_end(self, batch, logs=None):
        self.samples_seen += logs['size']
        self.write_value('train loss', self.writer, logs['loss'], self.samples_seen)

    def on_epoch_end(self, epoch, logs=None):
        # logs = {loss, val_loss}
        val_loss = logs.get('val_loss', None)

        if val_loss:
            self.write_value('validation loss', self.writer, val_loss, epoch)

        min_ratio, max_ratio, avg_ratio = self.ratios
        # ratios = self.mp_ratios
        # min_ratio, max_ratio, avg_ratio = np.min(ratios), np.max(ratios), np.average(ratios)
        self.write_value('ratio', self.writer_ratio_min, min_ratio, epoch)
        self.write_value('ratio', self.writer_ratio_max, max_ratio, epoch)
        self.write_value('ratio', self.writer_ratio_avg, avg_ratio, epoch)

    def write_value(self, name: str, writer: tf.summary.FileWriter, value: Any, index: int):
        summary = tf.Summary()
        summary_val = summary.value.add()
        summary_val.simple_value = value
        summary_val.tag = name
        writer.add_summary(summary, index)
        writer.flush()

    def on_train_end(self, _):
        self.writer.close()
        self.writer_ratio_avg.close()
        self.writer_ratio_max.close()
        self.writer_ratio_min.close()

    @property
    def ratios(self):
        min_ratio, max_ratio, avg_ratio = 0.0, 0.0, 0.0
        count: int = 0
        for context, expected in tqdm(self.validation_data(),
                                      'Computing distance ratio',
                                      self.data_count // self.bs,
                                      leave=False,
                                      unit='batch'):
            predicted = self.model.predict(context)
            ratio = np.sqrt(np.sum(np.power(predicted - expected, 2), 1))
            context = context.reshape((-1, self.nfeatures))
            context = context[context.shape[0] // 2]
            ratio /= np.sqrt(np.sum(np.power(context - expected, 2), 1))

            min_ratio = min_ratio if min_ratio <= np.min(ratio) else np.min(ratio)
            max_ratio = max_ratio if max_ratio >= np.max(ratio) else np.max(ratio)
            avg_ratio += np.sum(ratio)
            count += ratio.shape[0]
        avg_ratio /= count
        return min_ratio, max_ratio, avg_ratio

    @property
    def mp_ratios(self):
        raise NotImplementedError("Still not finished")
        with mp.Pool(self.nproc) as pool:
            with tqdm(enumerate(self.validation_data), total=self.data_count // self.bs) as data:
                ratios = pool.starmap(mp_ratio, ((
                    idx,
                    self.model,
                    context,
                    self.nfeatures,
                    expected,
                ) for idx, (context, expected) in data))
        return np.asarray(ratios).reshape(-1)
