import keras as k

from src import *

if __name__ == '__main__':

    model = AutoEncoder(
        ModelType.MLP,  # the only working for now
        optimizer=k.optimizers.Adam(lr=0.001, decay=0.0, amsgrad=True),
        bs=4096,
        valid_ratio=0.1,
        encoder_layers_config=[
            # encoder layers configuration
            dict(units=1000, activation=k.activations.relu),
            dict(units=500, activation=k.activations.relu),
            dict(units=250, activation=k.activations.relu),
            dict(units=100, activation=k.activations.relu),
            dict(units=50, activation=k.activations.relu),
        ],
        decoder_layers_config=[
            # decoder layers configuration
            dict(units=50, activation=k.activations.relu),
            dict(units=250, activation=k.activations.relu),
            dict(units=500, activation=k.activations.relu),
            dict(units=39, activation=k.activations.relu),
        ],
    )

    model.fit(100)
