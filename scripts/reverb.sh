#!/bin/bash
DIRSAMPLES=${1}
SUFFIX=${2}

mkdir -p ${DIRSAMPLES/samples/samples${SUFFIX}}

function changeName() {
    filepath="${1/samples/samples${2}}"
    if [[ ! -f ${filepath} ]]; then
        mkdir -p ${filepath%/*}
        echo "${1} -> adding reverb -> ${filepath}"
        sox -V1 ${1} ${filepath} reverb
    fi
}

export -f changeName
parallel changeName {} ${SUFFIX} -j $(nproc) ::: $(find ${DIRSAMPLES} -name '*.wav')
